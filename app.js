const express = require("express");
const user = require("./routes/user");
const ticket_booking = require("./routes/ticket_booking");
const flight_reschedule = require("./routes/flight_reschedule");
const app = express();
const mongoose = require("mongoose");
mongoose
    .connect("mongodb://localhost:27017/rolebased")
    .then(() => console.log("Connected to MongoDB..."))
    .catch((err) => console.error("Could not connect to MongoDB..."));
app.use(express.json());
app.use("/createuser", user);
app.use("/ticket_booking", ticket_booking);
app.use("/flight_reschedule", flight_reschedule);
const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on port ${port}...`));